﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using StudentManagement.Tables;
using StudentManagement.Services;
using Moq;
using System.Collections.Generic;
using StudentManagement.ViewModel;

namespace StudentProjTest
{
    [TestClass]
    public class UnitTest1
    {
       
       
        //private IUserServices _userService;

        //public void SetUP()
        //{
        //    var userRoles = new List<Role>
        //    {
        //        new Role {RoleID=1,RoleName="Admin"},
        //        new Role {RoleID=2,RoleName="User"},
        //        new Role {RoleID=3,RoleName="Super User"}
        //    };
        //    var service = new Mock<IUserServices>();
        //    service.Setup(x => x.GetRoleName(1)).Returns("Admin");
        //    _userService = service.Object;

        //}
        [TestMethod]
        public void LogInByAdmin()
        {
           var userService = new UserServices();
           var rolename= userService.GetRoleName(1);
           Assert.AreEqual("Admin", rolename);
        }

        [TestMethod]
        public void  GetStudentList()
        {
            var studentService = new StudentService();
            var model = new SearchViewModel()
            {
                Draw = 1,
                PageSize = 3,
                SortColumn = "FirstName",
                SortColumnDir = "asc"
            };
            var studentList = studentService.StudentList(model);
            Assert.IsTrue(studentList.Count > 0);
        }
    }
}
