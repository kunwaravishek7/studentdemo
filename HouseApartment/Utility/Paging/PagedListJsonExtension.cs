﻿

using StudentManagement.Utility;


namespace StudentManagement.Utility
{
    public static class PagedListJsonExtension
    {
        /// <summary>
        /// Converts IPagedList to a JSON object to be returned
        /// </summary>
        /// <typeparam name="T"><see cref="T"/></typeparam>
        /// <param name="source">IPagedList list</param>
        /// <returns></returns>
        public static PagedJson<T> ToJson<T>(this IPagedList<T> source)
        {
            var pagedData = new PagedJson<T>()
            {
                Data = source,
                PageNo = source.PageNo,
                PageSize = source.PageSize,
                TotalCount = source.TotalCount,
                TotalPages = source.TotalPages
            };
            return pagedData;
        }
    }
}
