﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Quantum.Data.Listing
{
    public static class OrderByExtension
    {
        /// <summary>
        /// Does Ordering for Single Property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">query</param>
        /// <param name="orderBy">property in <typeparamref name="T"/> to order by</param>
        /// <param name="orderType">desc or asc</param>
        /// <returns></returns>
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, string orderBy, string orderType )
        {
            if (!string.IsNullOrEmpty(orderBy))
            {
                orderBy = orderBy.Replace(" asc", "").Replace(" desc", "").Trim();
                var isDescending = string.IsNullOrEmpty(orderType)||orderType.ToLower() == "asc" ? false : orderType.ToLower() == "desc" || orderType.ToLower() == "descending";
                Type type = typeof(T);
                ParameterExpression arg = Expression.Parameter(type, "x");

                PropertyInfo pi = type.GetProperty(orderBy);
                Expression expr = Expression.Property(arg, pi);
                type = pi.PropertyType;

                Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
                LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);

                String methodName = isDescending ? "OrderByDescending" : "OrderBy";
                object result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == methodName
                            && method.IsGenericMethodDefinition
                            && method.GetGenericArguments().Length == 2
                            && method.GetParameters().Length == 2)
                    .MakeGenericMethod(typeof(T), type)
                    .Invoke(null, new object[] { source, lambda });
                source=(IQueryable<T>)result;
            }
            return source;
        }

        //TODO Ordering for Multiple Properties
    }
}
