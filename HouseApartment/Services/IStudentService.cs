﻿using StudentManagement.Utility;
using StudentManagement.ViewModel;
using StudentManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Services
{
   public interface IStudentService
    {
        bool CreateStudent(StudentViewModel model);
        StudentViewModel GetStudent(int  StudentID);
        bool EditStudent(StudentViewModel model);
        bool DeleteStudent (int StudentID);
        IPagedList<StudentViewModel> StudentList(SearchViewModel model);
    }
}
