﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentManagement.ViewModel;
using StudentManagement.Tables;
using StudentManagement.Tables;
using StudentManagement.ViewModel;
using StudentManagement.Utility;
using System.Linq.Dynamic;

namespace StudentManagement.Services
{
    public class StudentService : IStudentService
    {
        protected DataContext db;
        public StudentService()
        {
            db = new DataContext();
        }
        public bool CreateStudent(StudentViewModel model)
        {
            if (model != null)
            {
                var data = new Student()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Address = model.Address,
                    Class = model.Class,
                    DateOfBirth = model.DateOfBirth
                };
                db.Students.Add(data);
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteStudent(int StudentID)
        {
            var data = GetStudentDetails(StudentID);
            if (data != null)
            {
                db.Students.Remove(data);
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EditStudent(StudentViewModel model)
        {
            var IsDataExist = GetStudentDetails(model.StudentID);
            if (IsDataExist != null)
            {
                IsDataExist.FirstName = model.FirstName;
                IsDataExist.LastName = model.LastName;
                IsDataExist.Address = model.Address;
                IsDataExist.Class = model.Class;
                IsDataExist.DateOfBirth = model.DateOfBirth;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public StudentViewModel GetStudent(int StudentID)
        {
            var IsDataExist = GetStudentDetails(StudentID);
            if (IsDataExist != null)
            {
                var data = new StudentViewModel()
                {
                    FirstName = IsDataExist.FirstName,
                    LastName = IsDataExist.LastName,
                    Address = IsDataExist.Address,
                    Class = IsDataExist.Class,
                    DateOfBirth = IsDataExist.DateOfBirth,
                    StudentID=IsDataExist.StudentID
                };
                return data;
            }
            else
            {
                return null;
            }

        }

        public IPagedList<StudentViewModel> StudentList(SearchViewModel model)
        {
            var query = db.Students.Select(x => new StudentViewModel()
            {
                FirstName = x.FirstName,
                LastName = x.LastName,
                Address = x.Address,
                Class = x.Class,
                DateOfBirth = x.DateOfBirth,
                StudentID=x.StudentID
            }).AsQueryable();

            //search
            if (!string.IsNullOrEmpty(model.SearchValue))
            {
                query = query.Where(a =>

                    a.FirstName.Contains(model.SearchValue)
                    );
            }

            //sort
            if (!(string.IsNullOrEmpty(model.SortColumn) && string.IsNullOrEmpty(model.SortColumnDir)))
            {
                query = query.OrderBy(model.SortColumn + " " + model.SortColumnDir);
            }
            return query.ToList().ToPagedList(model.PageNo, model.PageSize);
        }

        private Student GetStudentDetails(int StudentID)
        {
            return db.Students.Where(x => x.StudentID == StudentID)
                 .FirstOrDefault();
        }
    }
}