﻿using StudentManagement.Tables;
using StudentManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Services
{
  public  interface IUserServices
    {
      UserViewModel  Insert(UserViewModel model);
        bool IsExistEmail(string EmailID);
        string GetRoleName(int roleid);
    }
}
