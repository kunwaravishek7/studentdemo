﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentManagement.ViewModel
{
    public partial class StudentViewModel
    {
        public int StudentID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int Class { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
    }
}