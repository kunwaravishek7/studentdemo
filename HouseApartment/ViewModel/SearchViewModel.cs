﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentManagement.ViewModel
{
    public class SearchViewModel
    {
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public string SearchValue { get; set; }
        public string SortColumn { get; set; }
        public string SortColumnDir { get; set; }
        public int Draw { get; set; }
    }

    public partial class ReturnList
    {
        public int Draw { get; set; }
        public int RecordFiltered { get; set; }
        public int RecordsTotal { get; set; }
        public List<UserViewModel> Users { get; set; }
    }
}