﻿using StudentManagement.ViewModel;
using StudentManagement.Services;
using StudentManagement.ViewModel;
using StudentManagement.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StudentManagement.Controllers
{
    [Authorize(Roles = "Admin")]
    public class StudentController : ApiController
    {
        private IStudentService _studentService;
        //public StudentController(IStudentService studentService)
        //{
        //    _studentService = studentService;
        //} //since implementation contains generic method: TODO


        public StudentController()
        {
            _studentService = new StudentService();
        }

        [HttpPost]
        [Route("api/student/create")]
        public IHttpActionResult Post([FromBody] StudentViewModel model)
        {
            var data = _studentService.CreateStudent(model);
            return Ok(data);
        }


        [HttpPost]
        [Route("api/student/getStudentList")]
        public IHttpActionResult StudentList([FromBody]SearchViewModel model)
        {
            var list = _studentService.StudentList(model);
            return Ok(list.ToJson());
        }



        [HttpGet]
        [Route("api/student/getStudentByID")]
        public IHttpActionResult StudentByID(int ID)
        {
            var data = _studentService.GetStudent(ID);
            return Ok(data);
        }


        [HttpPost]
        [Route("api/student/updateStudent")]
        public IHttpActionResult Update([FromBody]StudentViewModel model)
        {
            var updatedData = _studentService.EditStudent(model);
            return Ok(updatedData);
        }

        [HttpPost]
        [Route("api/student/deleteByStudentID")]
        public IHttpActionResult Delete([FromBody] int ID)
        {
            var IsDeleted = _studentService.DeleteStudent(ID);
            return Ok(IsDeleted);
        }

    }
}
