﻿var myApp = angular.module('myApp', ['ui.router', 'ui.bootstrap','datatables']);
//config routing
myApp.config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/login');
    $stateProvider
        .state('auth', {
            abstract: true,
            views: {
                '@': {
                    templateUrl: '/client/views/layouts/main.html'
                },
            }
        })
        .state('public', {
            abstract: true,
            views: {
                '@': {
                    templateUrl: '/client/views/layouts/authentication.html'
                }
            },
            data: {
                pageTitle: 'Home',
                requiredLogin: false
            }
        })          
          .state("auth.student", {
              url: '/student',
              templateUrl: '/client/views/student.html',
          })
          .state("auth.authorize", {
              url: '/authorize',
              templateUrl: '/client/views/authorize.html',
              controller: 'AuthorizeCtrl',

              data: { pageTitle: 'Dashboard' }
          })
            .state('public.login', {
                url: '/login',
                controller: 'LoginCtrl',
                templateUrl: '/client/views/login.html',
                controllerAs: 'vm'
            })
        .state('public.register', {
            url: '/register',
            controller: 'RegisterCtrl',
            templateUrl: '/client/views/register.html',
            controllerAs: 'vm'
        })
}])


//global veriable for store service base path
myApp.constant('serviceBasePath', 'http://localhost:3855');
//controllers

//http interceptor
myApp.config(['$httpProvider', function ($httpProvider) {
    var interceptor = function (userService, $q, $location) {
        return {
            request: function (config) {
                var currentUser = userService.GetCurrentUser();
                if (currentUser != null) {
                    config.headers['Authorization'] = 'Bearer ' + currentUser.access_token;
                }
                return config;
            },
            responseError: function (rejection) {
                if (rejection.status === 401) {
                    $location.path('/login');
                    return $q.reject(rejection);
                }
                if (rejection.status === 403) {
                    $location.path('/unauthorized');
                    return $q.reject(rejection);
                }
                return $q.reject(rejection);
            }

        }
    }
    var params = ['userService', '$q', '$location'];
    interceptor.$inject = params;
    $httpProvider.interceptors.push(interceptor);
}]);
