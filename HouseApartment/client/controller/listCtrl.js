﻿(function () {
    'use strict';
    myApp.controller('ListCtrl', ListCtrl);

    ListCtrl.$inject = ['$scope', 'postService', 'dataService', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', '$compile', '$uibModal', '$rootScope'];

    function ListCtrl($scope, postService, dataService, DTOptionsBuilder, DTColumnBuilder, $timeout, $compile, $uibModal, $rootScope) {
        var vm = this;
        vm.message = '';
        vm.someClickHandler = someClickHandler;
        $scope.pageData = {
            total: 0,
        };
        $scope.delete = function (id) {
            console.log('Deleting' + id);
            // Delete some data and call server to make changes...
            // Then reload the data so that DT is refreshed
            //return $http.delete('www.testtest.re/' + id).then(function () {
            // wait for the response to complete, or the deleted entry is still shown in the table
            //    $scope.dtOptions.reloadData();
            //});
        };
        $scope.edit = function (name) {
            var mynewscope = $rootScope.$new();
       //     $uibModal is a service to create modal windows. It has 'open' method, that will return modal instance.
            var modalInstance = $uibModal.open({
                templateUrl: '/client/views/editList.html',
                controller: function ($scope, items) { //defining controller here
                    $scope.Name = "Edit";
                    $scope.data = name;
                    $scope.selected = {
                        item: '......' //default selected item
                    };
                    $scope.ok = function () {
                        modalInstance.close($scope.selected.item);
                    };
                    $scope.cancel = function () {
                        modalInstance.dismiss('cancel');
                       // modalInstance.close($scope.selected.item);
                    }
    
                },
                resolve: {
                    items: function () {
                        return $scope.itemsOrganization;
                    }
                },// data passed to the controller
                scope: mynewscope
            }).result.then(function (data) {
                //do logic
            }, function () {
                // action on popup dismissal.
            });
            // wait for the response to complete, or the deleted entry is still shown in the table
            //    $scope.dtOptions.reloadData();
            //});
        };
        //$scope.dtInstanceCallback = function (instance) {
        //    $scope.dtInstance = instance;
        //};

        // this function used to get all leads
        var get = function (sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var order = aoData[2].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
        
            var search = aoData[5].value.value;
            var columns = aoData[1].value;
           // console.log(order)
            var sortColumn = columns[order[0].column].name;
            var sortColumnDir = order[0].dir;

    
            //var params  = {
            //    PageSize:start,
            //    PageNo:length,
            //}
            //postService.getListListing();
            //myService.get(params).$promise.then(function(response){
            postService.getListListing(start, length, search, sortColumn, sortColumnDir, draw).then(function (response) {
              //  console.log(sortColumn);
                //console.log(start, length)
                //var records = {
                //    'draw': 0,
                //    'recordsTotal': 0,
                //    'recordsFiltered': 0,
                //    'data': []
                //};
                //if(response.Data){
                //    records = {
                //        'draw': response.PageNo,
                //        'recordsTotal': response.TotalCount,
                //        'recordsFiltered': response.TotalCount,
                //        'data': response.Data
                //    };
                //}
                $scope.pageData.total = response.recordsTotal;
                fnCallback(response);
            });
        }
        function someClickHandler(info) {
         //   console.log(info);
            vm.message = info.Id;
       

        }
        function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //$compile(nRow)($scope);
            //$(nRow).attr('data-id', aData.id);
            $('td', nRow).unbind('click');
            $('td', nRow).bind('click', function () {
                $scope.$apply(function () {
                    vm.someClickHandler(aData);
                });
            });
            return nRow;
        }

        vm.dtOptions = DTOptionsBuilder.newOptions()
              .withFnServerData(get)
             .withDataProp('data')

         //  .withOption('processing', true) //for show progress bar
        .withOption('serverSide', true) // for server side processing
        .withPaginationType('full_numbers') // for get full pagination options // first / last / prev / next and page numbers
        .withDisplayLength(5) // Page size

        .withOption('aaSorting', [0, 'asc']) // for default sorting column // here 0 means first column
            //today
        .withOption('rowCallback', rowCallback)
        .withOption('createdRow', function (row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function (header) {
            if (!vm.headerCompiled) {
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        //    .withFnServerData(get) // method name server call
        //    .withDataProp('data')// parameter name of list use in getLeads Fuction
        //    .withOption('processing', true) // required
        //    .withOption('serverSide', true)// required
        //    .withOption('paging', true)// required
        //    .withPaginationType('full_numbers')
        //    .withDisplayLength(10)
        //    .withOption('rowCallback', rowCallback)
        //     .withDOM('lrtip')
        //.withOption('order', [1, 'asc'])
            //.withOption('lengthMenu', [50, 100, 150, 200])
        vm.dtColumns = [
        DTColumnBuilder.newColumn("Name", "Name").withOption('name', 'Name'),
             //DTColumnBuilder.newColumn('Name', 'Name').withTitle('Name').withOption('searchable', true),
         DTColumnBuilder.newColumn('Description', 'Description').withOption('name', 'Description'),
   
    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
        .renderWith(function (data, type, full, meta) {
            //console.log(data)
            var id = full.Id;
            var str = "\'" + data.Id + "\'"
     
            return '<button class="btn btn-warning" ng-click="edit(' + data.Name + ')">' +
                '   <i class="fa fa-edit"></i>' +
                '</button>&nbsp;' +
                '<button class="btn btn-danger" ng-click="delete(' + str + ')">' +
                '   <i class="fa fa-trash-o"></i>' +
                '</button>';
        })
        ];

        //$('#tableid').on('click', 'tbody td:not(.no-click)', function () {
        //    var id = $(this).parent().attr('data-id');
        //    vm.message = id;
        //    $location.path('info/' + id);
        //})
    }


})();

