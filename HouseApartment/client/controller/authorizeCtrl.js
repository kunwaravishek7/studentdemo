﻿/// <reference path="C:\Users\Avishek\Desktop\final\finalProjectStudentManagement\StudentManagement\js/plugins/bootstrap-notify.min.js" />

(function () {
    'use strict';
    myApp.controller('AuthorizeCtrl', authorizeController);
    authorizeController.$inject = ['$scope', 'dataService'];
    function authorizeController($scope, dataService) {
        $scope.data = "";
        dataService.GetAuthorizeData().then(function (data) {
            $scope.data = data.details;
            $scope.name = data.name;

            //notifications
            var notificationHub = $.connection.notificationHub;
            notificationHub.client.sendMessage = function (message) {
                $.notify({
                    title: "",
                    message: message,
                    icon: 'fa fa-check'
                }, {
                    type: "success"
                });
            };
            $.connection.hub.start().done(function () {
                //RRM: Register All online users
                notificationHub.server.registerClient(data.name);
                notificationHub.server.message("Hello Everyone!", data.name);
            });
        })
    }
})();

