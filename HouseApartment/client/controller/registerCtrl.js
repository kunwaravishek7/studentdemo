﻿(function () {
    'use strict';
    myApp.controller('RegisterCtrl', registerController);

    registerController.$inject = ['$scope', 'dataService','$location'];

    function registerController($scope, dataService, $location) {

        var vm = this;
        vm.CreateUser = {};
        $scope.Create = function (model) {
            dataService.CreateUser(model).then(function (data) {
                $.notify({
                    title: "",
                    message: "Your Confirmation Link Has been sent to: "+data,
                    icon: 'fa fa-check'
                }, {
                    type: "info"
                });
                $location.path('/login');
            })


        }

        $scope.loginpage = function () {
            $location.path('/login');
        };
    
    }
})();
