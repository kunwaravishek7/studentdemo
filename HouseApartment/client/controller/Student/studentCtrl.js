﻿(function () {
    'use strict';
    myApp.controller('StudentCtrl', StudentCtrl);


    StudentCtrl.$inject = ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnBuilder', '$compile', 'StudentService', '$uibModal', '$rootScope','$filter'];

    function StudentCtrl($scope, $http, DTOptionsBuilder, DTColumnBuilder, $compile, StudentService, $uibModal, $rootScope, $filter) {
        var vm = this;

        $scope.pageData = {
            total: 0,
        };

        vm.dtInstance = {};

        //create student records
        vm.Create = function () {
            var mynewscope = $rootScope.$new();
            //     $uibModal is a service to create modal windows. It has 'open' method, that will return modal instance.
            var modalInstance = $uibModal.open({
                templateUrl: '/client/views/addStudent.html',
                controller: function ($scope, items) { //defining controller here
                    $scope.Title = "Create Student"
                    $scope.Create = {};

                    $scope.ClassList =
                      [
                        { Name: "8", Value: 8 },
                        { Name: "9", Value: 9 },
                        { Name: "10", Value: 10 }
                      ];


                    $scope.selected = {
                        item: '......' //default selected item
                    };
                    $scope.ok = function () {
                        modalInstance.close($scope.selected.item);
                    };
                    $scope.cancel = function () {
                        modalInstance.dismiss('cancel');
                        // modalInstance.close($scope.selected.item);
                    }

                    $scope.CreateStudent = function (data) {
                        StudentService.Create(data).then(function (data) {

                            if (data) {
                                $.notify({
                                    title: "",
                                    message: "Successfully Created!",
                                    icon: 'fa fa-check'
                                }, {
                                    type: "info"
                                });
                                vm.dtInstance.rerender();
                            }

                        })
                    };


                },
                resolve: {
                    items: function () {
                        return $scope.StudentID;
                    }
                },// data passed to the controller
                scope: mynewscope
            }).result.then(function (data) {
                //do logic
            }, function () {
                // action on popup dismissal.
            });
            // wait for the response to complete, or the deleted entry is still shown in the table
            //    $scope.dtOptions.reloadData();
            //});
        };

        //update student records
        $scope.edit = function (ID) {
            var mynewscope = $rootScope.$new();
            var modalInstance = $uibModal.open({
                templateUrl: '/client/views/editStudent.html',
                controller: function ($scope, items) { //defining controller here
                    $scope.Title = "Edit User"
    

                    StudentService.StudentByID(ID).then(function (data) {
                        console.log(data)
                        $scope.Edit = data;
                        $scope.EditClassList =
                        [
                          { Name: "8", Value: 8 },
                        { Name: "9", Value: 9 },
                        { Name: "10", Value: 10 }
                        ];
                        var date = new Date($scope.Edit.DateOfBirth);

                        var $datepicker = $('#demoDate');
                        $datepicker.datepicker();
                        $datepicker.datepicker('setDate', new Date(date));
                    })



                    $scope.selected = {
                        item: '......' //default selected item
                    };
                    $scope.ok = function () {
                        modalInstance.close($scope.selected.item);
                    };
                    $scope.cancel = function () {
                        modalInstance.dismiss('cancel');
                        // modalInstance.close($scope.selected.item);
                    }

                    $scope.UpdateStudentRecords = function (data) {
                        StudentService.Update(data).then(function (data) {
                            if (data) {
                                vm.dtInstance.rerender();
                                //   vm.dtInstance
                                $.notify({
                                    title: "",
                                    message: " Student Record Updated!",
                                    icon: 'fa fa-check'
                                }, {
                                    type: "info"
                                });
                            }

                        })
                    }


                },
                resolve: {
                    items: function () {
                        return $scope.StudentID;
                    }
                },// data passed to the controller
                scope: mynewscope
            }).result.then(function (data) {
                //do logic
            }, function () {
                // action on popup dismissal.
            });
            // wait for the response to complete, or the deleted entry is still shown in the table
            //    $scope.dtOptions.reloadData();
            //});
        };

        $scope.delete = function (ID) {
            var result = confirm("Are you sure, you want to delete?");
            if (result) {
                StudentService.Delete(ID).then(function (data) {
                    if (data) {
                        $.notify({
                            title: "",
                            message: " Student Record Removed!",
                            icon: 'fa fa-check'
                        }, {
                            type: "info"
                        });
                        vm.dtInstance.rerender();

                    }

                });
            }

        }

        var get = function (sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var order = aoData[2].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var search = aoData[5].value.value;
            var columns = aoData[1].value;
            var sortColumn = columns[order[0].column].name;
            var sortColumnDir = order[0].dir;

            StudentService.StudentList(draw, start, length, search, sortColumn, sortColumnDir).then(function (response) {
                $scope.pageData.total = response.recordsTotal;
                fnCallback(response);
            });
        }
        function actionsHtml(data, type, full, meta) {
            return '<button class="btn btn-warning" ng-click="edit(' + data.StudentID + ')">' +
                '   <i class="fa fa-edit"></i>' +
                '</button>&nbsp;' +
                '<button class="btn btn-danger" ng-click="delete(' + data.StudentID + ')">' +
                '   <i class="fa fa-trash-o"></i>' +
                '</button>';
        }
        function createdRow(row, data, dataIndex) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        }



        vm.dtOptions = DTOptionsBuilder.newOptions()
            .withFnServerData(get)
           .withDataProp('data')
      //  .withOption('processing', true) //for show progress bar
        .withOption('serverSide', true) // for server side processing

        .withPaginationType('full_numbers') // for get full pagination options // first / last / prev / next and page numbers
        .withDisplayLength(3) // Page size
        .withOption('aaSorting', [0, 'asc']) // for default sorting column // here 0 means first column
        .withOption('createdRow', createdRow)
        .withOption('createdRow', function (row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
        });
        vm.dtColumns = [
         //here We will add .withOption('name','column_name') for send column name to the server 
         DTColumnBuilder.newColumn("FirstName", "FirstName").withOption('name', 'FirstName'),
         DTColumnBuilder.newColumn("LastName", "LastName").withOption('name', 'LastName'),
           DTColumnBuilder.newColumn("Class", "Class").withOption('name', 'Class'),
           DTColumnBuilder.newColumn("Address", "Address").withOption('name', 'Address'),
                       DTColumnBuilder.newColumn("DateOfBirth", "DateOfBirth").withOption('name', 'DateOfBirth').renderWith(function(data, type) {
                           return $filter('date')(data, 'MM/dd/yyyy');   // Angularjs date filter
                       }),

         DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
         .renderWith(actionsHtml)
        ]

        vm.reloadData = function () {
            vm.dtInstance.rerender();
        }
    }


})();