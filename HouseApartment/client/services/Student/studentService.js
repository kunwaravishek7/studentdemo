﻿(function () {
    'use strict';
    myApp.service('StudentService', StudentService);

    StudentService.$inject = ['$http', '$q', 'serviceBasePath'];

    function StudentService($http, $q, serviceBasePath) {
        var service = {
            StudentList: StudentList,
            StudentByID: StudentByID,
            Update: Update,
            Delete: Delete,
            Create: Create
        };

        return service;

        var config = {
            headers: { 'Content-Type': 'application/json' }
        }


        function StudentList(draw, pageno, pagesize, SearchValue, SortColumn, SortColumnDir) {

            var data = {
                PageNo: pageno,
                PageSize: pagesize,
                SearchValue: SearchValue,
                SortColumn: SortColumn,
                SortColumnDir: SortColumnDir,
                Draw: draw
            };
            var deferred = $q.defer();
            $http.post('/api/student/getStudentList', data, config).success(function (response) {
                var
               records = {
                   'draw': draw,
                   'recordsTotal': response.TotalCount,
                   'recordsFiltered': response.TotalCount,
                   'data': response.Data
               };
                deferred.resolve(records);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;

        }

      

        function StudentByID(StudentID) {
            var config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            var deferred = $q.defer();
            $http.get('/api/student/getStudentByID', { params: { ID: StudentID } }, config).success(function (response) {

                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;

        }


        function Update(model) {
            var deferred = $q.defer();
            $http.post('/api/student/updateStudent', model, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;

        }


        function Delete(id) {
            var deferred = $q.defer();
            $http.post('/api/student/deleteByStudentID', id, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;

        }


        function Create(model) {

            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.post(serviceBasePath + '/api/Student/create', model, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;

        }
       
    }
})();