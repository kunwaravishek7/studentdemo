﻿using StudentManagement.Authorization;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.Owin.Host.SystemWeb;
using System.Threading;
using System.Web;

namespace StudentManagement.Notification
{

    public class NotificationHub : Hub
    {
   
       

        private static Dictionary<string, dynamic> connectedClients = new Dictionary<string, dynamic>();


        public void RegisterClient(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                lock (connectedClients)
                {
                    if (connectedClients.ContainsKey(userName))
                    {
                        connectedClients[userName] = Clients.Caller;
                    }
                    else
                    {
                        connectedClients.Add(userName, Clients.Caller);
                    }
                }
            }
        }


        public void message(string message, string clientid)
        {
            dynamic client = null;

            if (!string.IsNullOrEmpty(message))
            {


                foreach (var item in connectedClients)
                {
                    if (!string.IsNullOrEmpty(item.Key))
                    {
                        string user = item.Key;
                        if (clientid != user)
                        {
                            client = connectedClients[user];
                            client.sendMessage(message);
                        }
                        

                    }

                }
            }
        }
    }
}